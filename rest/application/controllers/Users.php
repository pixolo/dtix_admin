<?php 
 defined('BASEPATH') OR exit('No direct script access allowed'); 
 
 header('Access-Control-Allow-Origin: *'); 
 
 class Users extends PIXOLO_Controller { 
 
 	 function __construct(){ 
 	 	 parent::__construct(); 
 
 	 	 $this->load->model('User_Model', 'model'); 
 	 } 

 	 public function index() 
 	 { 
 	 	 $message['json']=$this->model->get_all(); 
 	 	 $this->load->view('json', $message); 
 	 } 

 	 public function dologin()
 	 {
 	     $email = $this->input->post('email');
 	     $password = $this->input->post('password');
 	     $message['json']=$this->model->dologin($email, $password);
 	     $this->load->view('json', $message);
 	 }

 	 public function checkuserandadd(){
         $contact = $this->input->get("contact");
         $email = $this->input->get("email");
         $device_id = $this->input->get("device_id");
         $category = $this->input->get("category");
         $userdevice = $this->input->get("user_device");
         $message['json']=$this->model->checkuserandadd($contact,$email, $category, $device_id,$userdevice);
         $this->load->view('json', $message);
     }
 }