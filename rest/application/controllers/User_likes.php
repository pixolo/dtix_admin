<?php 
 defined('BASEPATH') OR exit('No direct script access allowed'); 
 
 header('Access-Control-Allow-Origin: *'); 
 
 class User_likes extends PIXOLO_Controller { 
 
 	 function __construct(){ 
 	 	 parent::__construct(); 
 
 	 	 $this->load->model('User_like_model', 'model'); 
 	 } 

 	 public function index() 
 	 { 
 	 	 $message['json']=$this->model->get_all(); 
 	 	 $this->load->view('json', $message); 
 	 } 
 	 public function unlike(){
 	     $post_id=$this->input->get('post_id');
 	     $user_id=$this->input->get('user_id');
 	     $message['json']=$this->model->unlike($post_id,$user_id);
        $this->load->view('json', $message);
 	     
 	 }
 }