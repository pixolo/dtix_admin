<?php 
 defined('BASEPATH') OR exit('No direct script access allowed'); 
 
 header('Access-Control-Allow-Origin: *'); 
 
 class Notifications extends PIXOLO_Controller { 
 
 	 function __construct(){ 
 	 	 parent::__construct(); 
 
 	 	 $this->load->model('Notification_model', 'model'); 
 	 } 

 	 public function index() 
 	 { 
 	 	 $message['json']=$this->model->get_all(); 
 	 	 $this->load->view('json', $message); 
 	 } 
 	 
 	 public function sendnotificationtoall(){
	     	$message = $this->input->get('message');
	     	$id=$this->input->get('id');
	     	$this->model->sendnotificationtoall($message,$id);
	 }
	     
	     
 }

