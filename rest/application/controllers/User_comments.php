<?php 
 defined('BASEPATH') OR exit('No direct script access allowed'); 
 
 header('Access-Control-Allow-Origin: *'); 
 
 class User_comments extends PIXOLO_Controller { 
 
 	 function __construct(){ 
 	 	 parent::__construct(); 
 
 	 	 $this->load->model('User_comment_model', 'model'); 
 	 } 

 	 public function index() 
 	 { 
 	 	 $message['json']=$this->model->get_all(); 
 	 	 $this->load->view('json', $message); 
 	 }
 	 
 	 
 	 public function getcomments(){
 	     $post_id=$this->input->get('post_id');
 	     $message['json']=$this->model->getcomments($post_id);
        $this->load->view('json', $message);
 	     
 	 }
 	 
 	 
 	 
 }
 ?>