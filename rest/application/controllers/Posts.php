<?php 
 defined('BASEPATH') OR exit('No direct script access allowed'); 
 
 header('Access-Control-Allow-Origin: *'); 
 
 class Posts extends PIXOLO_Controller { 
     
      //public $inserttype = "post";
 
 	 function __construct(){ 
 	 	 parent::__construct(); 
 
 	 	 $this->load->model('Post_model', 'model'); 
 	 	 $this->inserttype = "post";
 	 	
 	 
 	 
 	 } 

 	 public function index() 
 	 { 
 	     	
 	 	 $message['json']=$this->model->get_all(); 
 	 	 $this->load->view('json', $message); 
 	 } 
 	 
 	 public function gettenofeach(){
 	     $message['json']=$this->model->gettenofeach(); 
 	 	 $this->load->view('json', $message); 
 	 }
 	 
 	  public function getfeatured(){
 	      
 	     $message['json']=$this->model->getfeatured(); 
 	 	 $this->load->view('json', $message); 
 	 }
 	 public function gettodaysbroadcast(){
 	      $message['json']=$this->model->gettodaysbroadcast(); 
 	 	 $this->load->view('json', $message); 
 	     
 	     
 	 }
 	 
 	 

 	 
 }