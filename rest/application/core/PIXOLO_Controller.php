<?php

class PIXOLO_Controller extends CI_Controller {
    
    public $data = array();
    
    function __construct(){
        parent::__construct();
         
    }

    public function insert()
	{
		$data = $this->input->get('data');
		$message['json']=$this->model->insert(json_decode($data));
		$this->load->view('json', $message);
		//CHECK IF THE INSERT IS FOR A POST ADN THE POST IS FOR TODAY
		if(isset($this->inserttype)){
		   if($this->inserttype == 'post'){
		    if(json_decode($data)->datetime == date("Y-m-d")){
		        //IF THE TIME IS AFTER 10:30 (10*60 + 30) = 630
		        if(((date('H')*60) + date('i')) >= 630){
    		        $this->makefeaturedjson();
    		        $this->sendtodaysnotification(json_decode($data)->newspaper_headline);
		        };
		    };
		}
		}

			  
	}

    public function postinsert()
    {
        $data = $this->input->post('data');
        $message['json']=$this->model->insert(json_decode($data));
        $this->load->view('json', $message);
    }

	public function getall()
	{
	  
		$message['json']=$this->model->get_all();
		$this->load->view('json', $message);
	}

	public function getalldesc()
	{
		$col = $this->input->get('column');
		$message['json']=$this->model->get_all_desc($col);
		$this->load->view('json', $message);
	}

	public function getbyid()
	{
		$id = $this->input->get('id');
		$message['json']=$this->model->get($id);
		$this->load->view('json', $message);
	}

	public function getoneby()
	{
		$field = $this->input->get('field');
		$value = $this->input->get('value');
		$message['json']=$this->model->get_by($field, $value);
		$this->load->view('json', $message);
	}

	public function getmanyby()
	{
		$field = $this->input->get('field');
		$value = $this->input->get('value');
		$message['json']=$this->model->get_many_by($field, $value);
		$this->load->view('json', $message);
	}

	public function getmanybytwo(){
		$field1 = $this->input->get('field1');
		$value1 = $this->input->get('value1');
		$field2 = $this->input->get('field2');
		$value2 = $this->input->get('value2');
		$message['json']=$this->model->get_many_by_two($field1, $value1, $field2, $value2);
		$this->load->view('json', $message);
	}

	public function getmanybythree(){
		$field1 = $this->input->get('field1');
		$value1 = $this->input->get('value1');
		$field2 = $this->input->get('field2');
		$value2 = $this->input->get('value2');
		$field3 = $this->input->get('field3');
		$value3 = $this->input->get('value3');
		$message['json']=$this->model->get_many_by_three($field1, $value1, $field2, $value2, $field3, $value3);
		$this->load->view('json', $message);
	}

	public function updatebyid()
	{
		$id = $this->input->get('id');
		$data = $this->input->get('data');
		$message['json']=$this->model->update($id, json_decode($data));
		$this->load->view('json', $message);
	}

    public function postupdatebyid()
    {
        $id = $this->input->post('id');
        $data = $this->input->post('data');
        $message['json']=$this->model->update($id, json_decode($data));
        $this->load->view('json', $message);
    }


	public function updatemanyby()
	{
		$field = $this->input->get('field');
		$value = $this->input->get('value');
		$data = $this->input->get('data');
		$message['json']=$this->model->update_by($field, $value, json_decode($data));
		$this->load->view('json', $message);
	}

	public function deletebyid()
	{
		$id = $this->input->get('id');
		$message['json']=$this->model->delete($id);
		$this->load->view('json', $message);
	}

	public function deletemanyby()
	{
		$field = $this->input->get('field');
		$value = $this->input->get('value');
		$message['json']=$this->model->delete_by($field, $value);
		$this->load->view('json', $message);
	}

	public function uploadImage(){

        $path = $this->input->post('path');

        if ( !empty( $_FILES ) ) {
            if(isset($_FILES["image"])){
                $image= $_FILES["image"]["tmp_name"];
                if($_FILES["image"]["size"] > 0)
                {
                    $_FILES['image']['name'] = str_replace(" ","-",$_FILES['image']['name']);
                    $name = preg_replace('/\s+/', '', $_FILES['image']['name']);
                    $name = preg_replace('/\(+/', '', $name);
                    $name = preg_replace('/\)+/', '', $name);
                    $new_name = strtolower('dtix'.time().$name);
                    $tempPath = $_FILES['image']['tmp_name'];
                    $uploadPath = FCPATH.'uploads/'.$path.'/'.$new_name;
                    move_uploaded_file( $tempPath, $uploadPath );
                    $value=0;
                    print_r($new_name);
                }
            }
        }
    }

    public function deleteImage(){

        $path = $this->input->post('path');

        $file_name = $this->input->post('filename');

        $filePath = FCPATH.'uploads/'.$path.'/'.$file_name;

        unlink($filePath);

        print_r("true");

    }

    public function databasebackup(){

        //PARAMETERS
        $download = $this->input->get("download");

        // Load the DB utility class
        $this->load->dbutil();

        // Backup your entire database and assign it to a variable
        $backup = $this->dbutil->backup();

        // Load the file helper and write the file to your server
        $this->load->helper('file');
        $file_name = time().".zip";
        write_file(FCPATH.'/database/'.$file_name, $backup);

        // Load the download helper and send the file to your desktop
        if($download == 'true') {
            $this->load->helper('download');
            force_download($file_name, $backup);
        };

    }
     public function makefeaturedjson(){
         
         
          if($this->input->get('callfrom')!=null){
             $callfrom =$this->input->get('callfrom');
              
          }else{
              
              $callfrom = $this->inserttype;
          }
 	  //  if(isset($this->inserttype)){
 	  //      $callfrom = $this->inserttype;
 	  //  }else{
 	  //      $callfrom = 'cron';
 	  //  };
 	   
 	     $message['json']=$this->model->makefeaturedjson($callfrom);
 	 	 $this->load->view('json', $message); 
 	 }
 	 
 	 public function sendtodaysnotification($msg){
 	     $this->model->sendtodaysnotification($msg);
 	 }
}