<?php 
 defined('BASEPATH') OR exit('No direct script access allowed'); 
 
 class Notification_model extends PIXOLO_Model 
 { 

 
 	 //Write functions here
 	 public function sendnotificationtoall($message,$id){
          	$query = $this->db->query("SELECT `deviceid` FROM `user_deviceid` WHERE `deviceid` != '0'")->result();
          	
           	$deviceid=array();
        
        	foreach ($query as $key => $row){
			    array_push($deviceid,$row->deviceid);  
          	};
          	
           	$response = $this->notification($deviceid,$message,$id);
        	print_r('Response<br>'.$response);
 	 }
 	 
 	 
 	 public function notification($deviceid,$message,$id)
       {
       
       	$app_info=array("info_id"=>$id);
              $content = array(
			"en" => $message
			);
      
	
		$fields = array(
			'app_id' => "23cf1711-f90d-4815-84ba-5d097fda1d4a",
			'included_segments' => array(
            'All'
        ),
			'contents' =>$content,
			'data' => $app_info,
		);
		
		
		$fields = json_encode($fields);
    	print("\nJSON sent:\n");
    	print($fields);
		
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json; charset=utf-8','Authorization: Basic ZDUwYzM5MzctMGVkZS00ZmNkLWI1MGQtNGI4Yjc3NzJlYWE2'));
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
			curl_setopt($ch, CURLOPT_HEADER, FALSE);
			curl_setopt($ch, CURLOPT_POST, TRUE);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

			$response = curl_exec($ch);
			curl_close($ch);
			print_r($response);
			return $response;
	}
 } 
 
 ?>