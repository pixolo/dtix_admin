<?php 
 defined('BASEPATH') OR exit('No direct script access allowed'); 
 
 class Post_model extends PIXOLO_Model 
 { 

 
 	 //Write functions here 
 	 public function gettenofeach(){
 	     $posts = new StdClass();
 	     $posts->ne = $this->db->query("SELECT *,IF(DATE(`datetime`) = CURDATE(), 'YES', 'NO') AS `newpost` FROM `posts` WHERE (`posttype_id`='1' OR `posttype_id`='2') AND `datetime` <= CURDATE() ORDER BY `datetime` DESC LIMIT 0,4")->result();
 	      $posts->an = $this->db->query("SELECT *,IF(DATE(`datetime`) = CURDATE(), 'YES', 'NO') AS `newpost` FROM `posts` WHERE (`posttype_id`='3' OR `posttype_id`='4') AND `datetime` <= CURDATE() ORDER BY `datetime` DESC LIMIT 0,4")->result();
 	     return $posts;
 	 }
 	 
 	 
 	 public function getfeatured(){
     $query = $this->db->query("SELECT * FROM `posts` WHERE `datetime` <= DATE(CONVERT_TZ(now(),'+00:00','+05:31')) ORDER BY `datetime` DESC LIMIT 0,4")->result();
     return $query;
}


public function gettodaysbroadcast(){
    
    
     $query=$this->db->query("SELECT p.id,p.datetime,CONCAT('http://dtix.org/dtix_admin/rest/uploads/posts/',p.image) AS image,p.catchy_words,p.series,p.newspaper_headline,p.hashtags,p.source_url,p.source_text,p.sponsor_url,p.sponsor_text,p.time,(SELECT COUNT(*) FROM user_likes WHERE post_id=p.id) AS likes,(SELECT COUNT(*) FROM user_comments WHERE post_id=p.id) AS comments, pt.name AS post_type FROM posts AS p LEFT JOIN posttypes AS pt ON p.posttype_id=pt.id WHERE `p`.`datetime` <= DATE(CONVERT_TZ(now(),'+00:00','+05:31')) ORDER BY `p`.`datetime` DESC LIMIT 0,4")->result();
 	   return $query;
}
 	 

 	 
 	 
 } 
 
 ?>