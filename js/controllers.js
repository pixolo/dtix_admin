var phonecatControllers = angular.module('phonecatControllers', ['templateservicemod', 'navigationservice']);

// //var adminurl = "http://localhost/rest/rest/index.php/";
// //var imageurl = "http://localhost/rest/rest/uploads/";
// var adminurl = "http://learnwithinq.com/adminpanel/rest/index.php/";
var imageurl = "http://dtix.org/dtix_admin/rest/uploads/";

var userarray = [{
	'image': 'admin.png',
	'post': 'Administrator'
}];

phonecatControllers.controller('home', ['$scope', 'TemplateService', 'NavigationService', '$rootScope', '$filter', '$window', '$location',
  function ($scope, TemplateService, NavigationService, $rootScope, $filter, $window, $location) {
		$scope.template = TemplateService;
		TemplateService.content = "views/content.html";
		$scope.title = "dashboard";
		$scope.navigation = NavigationService.getnav();


  }
]);

phonecatControllers.controller('loginCtrl', ['$scope', 'TemplateService', 'NavigationService', '$rootScope', '$location',
  function ($scope, TemplateService, NavigationService, $rootScope, $location) {
	/*	$scope.template = TemplateService;
		TemplateService.content = "views/login.html";
		$scope.title = "dashboard";*/
		// Initializations
		$scope.adminpaneluser = {};
		console.log("LOGIN CONTROLLER>>>>>>>>>>>");
		// callbacks
		loginsuccess = function (response) {
			console.log(response);
			if (response.data && response.data!='false') {
				var userdata = {};
				userdata.id = response.data.id;
				userdata.name = response.data.name;
				userdata.email = response.data.email;
				userdata.access = response.data.access;
				$.jStorage.set('dtix_user', userdata);
				$location.path('/users');
			} else {
				$scope.errormessage = "Email ID and password didn't match !";
			}
		}

		loginerror = function (error) {
			console.log('Internet Error');

		}

		// Scope FUNCTIONS
		$scope.login = function () {
			console.log($scope.adminpaneluser);
			if ($scope.adminpaneluser.email && $scope.adminpaneluser.email != '' && $scope.adminpaneluser.password && $scope.adminpaneluser.password != '') {
				console.log('In if');
				NavigationService.login($scope.adminpaneluser).then(loginsuccess, loginerror);

			} else {
				console.log("All fields are required !");
			}

			// console.log('called');


		}


  }
]);

phonecatControllers.controller('usersCtrl', ['$scope', 'TemplateService', 'NavigationService', '$location',
  function ($scope, TemplateService, NavigationService, $location) {
		$scope.template = TemplateService;
		TemplateService.content = "views/users.html";
		$scope.title = "usersCtrl";

		// Initializations
		$scope.users = [];
		$scope.hidetextfields = true;

		// CallBacks


		getalldatasuccess = function (response) {
			console.log(response);
			$scope.users = _.map(response.data, function (element) {
				Object.defineProperty(element, 'name', {
					writable: false
				});
				return element;
			});
		}
		getalldataerror = function (error) {
			console.log('Internet Error');
		}
		editdatasuccess = function (response) {
			console.log(response);
		}
		editdataerror = function (error) {
			console.log('Internet Error');
		}

		deletedatabyidsuccess = function (response) {

			console.log(response);
		};
		deletedatabyiderror = function (error) {
			console.log('Internet Error');
		};

		// Scopes


		$scope.savetournament = function (newtournament) {
			if (newtournament.name) {
				if (newtournament.id) {
					NavigationService.editdata('tournaments', {
						"id": newtournament.id,
						"data": angular.toJson(newtournament)
					}).then(editdatasuccess, editdataerror);
					newtournament.notdisabled = true;
					Object.defineProperty(newtournament, 'name', {
						writable: false
					});
				} else {
					NavigationService.storepostdata('tournaments', newtournament).then(function (response) {
						console.log(response);
						newtournament.id = response.data;
						Object.defineProperty(newtournament, 'name', {
							writable: false
						});
					}, function (error) {

					});

				}
			}
		};


		$scope.deletetournament = function (tournament) {

			NavigationService.deletedatabyid('tournaments', tournament.id).then(function (response) {
				console.log(response);
				$scope.tournaments.splice($scope.tournaments.indexOf(tournament), 1);
			}, function (error) {

			});
		};

		$scope.addnewtournament = function () {
			$scope.tournaments.push({
				name: ''
			});
			Object.defineProperty($scope.tournaments[$scope.tournaments.length - 1], 'name', {
				writable: true
			});
		}

		$scope.canceladdition = function (tournament) {
			if (tournament.id) {
				console.log(originaldata[tournament.id]);
				tournament.name = originaldata[tournament.id].name;
				tournament.primary_color = originaldata[tournament.id].primary_color;
				tournament.secondary_color = originaldata[tournament.id].secondary_color;



				console.log($scope.tournaments[$scope.tournaments.indexOf(tournament)]);
				Object.defineProperty(tournament, 'name', {
					writable: false
				});
			} else {
				var index = $scope.tournaments.indexOf(tournament);
				$scope.tournaments.splice(index, 1);
			}

		}
		$scope.edittournament = function (tournament) {
			originaldata[tournament.id] = angular.copy(tournament);
			Object.defineProperty(tournament, 'name', {
				writable: true
			});
			console.log(tournament);

		}

		// Navigation services

		NavigationService.getalldata('users').then(getalldatasuccess, getalldataerror);

		$scope.gotocategorypage = function (tournamentid) {
			$.jStorage.set('tournament', tournamentid)
			$location.path('categories/' + tournamentid);
		}
  }
]);

/*LOGIN CONTROLLER*/

//phonecatControllers.controller('loginCtrl', ['$scope', 'TemplateService', 'NavigationService', '$routeParams', '$location',
//  function ($scope, TemplateService, NavigationService, $routeParams, $location) {
//		$scope.template = TemplateService;
//		TemplateService.content = "views/login.html";
//		$scope.title = "login";
//		$scope.user = {
//			email: '',
//			password: ''
//		};
//
//
//		/*CALLBACK FUNCTIONS*/
//		loginsuccess = function (response) {
//			console.log(response);
//		}
//		loginerror = function (error) {
//			console.log(error);
//		}
//		
//		
//		/*SCOPE FUNCTIONS*/
//		$scope.dologin = function () {
//			console.log($scope.user);
//			if ($scope.user.email&&$scope.user.email != '' &&$scope.user.password&& $scope.user.password != '') {
//				console.log('In if');
//				NavigationService.login($scope.user).success(loginsuccess).error(loginerror);
//
//			} else {
//				console.log("All fields are required !");
//			}
//
//
//
//		}
//
//
//
//
//
//	 }
//]);

phonecatControllers.controller('postsCtrl', ['$scope', 'TemplateService', 'NavigationService', '$routeParams', '$location',
  function ($scope, TemplateService, NavigationService, $routeParams, $location) {
		$scope.template = TemplateService;
		TemplateService.content = "views/posts.html";
		$scope.title = "posts";
        var postidtobedeleted='';
		// Initializations
		var originaldata = {};

		// Callbacks Functions
		getalldatasuccess = function (response) {
			console.log(response);
			$scope.posts = response.data;
		}
		getalldataerror = function (error) {
			console.log('Internet Error');
		}


		// Scope functions
		$scope.gotocreatepost = function () {
			$location.path("createpost/:0");
		};

		//GO TO EDIT PAGE
		$scope.gotoedit = function (id) {
			$location.path("createpost/" + id);
		};

		//DELETE POST
		var deletepostsuccess = function (response) {
			console.log(response.data);
			NavigationService.getalldatadesc('posts', 'datetime').then(getalldatasuccess, getalldataerror);
		};
		var deleteposterror = function (response) {
			console.log(response.data);
		};
		$scope.deletepost = function () {
			NavigationService.deletedatabyid('posts', postidtobedeleted).then(deletepostsuccess, deleteposterror);
            $(".popup-wrapper").css("display","none");
		};
      
        $scope.deleteconfirmation = function (postid) {
            postidtobedeleted=postid;
            $(".popup-wrapper").css("display","block");
        };
      
        $scope.hideconfirmation = function () {
            $(".popup-wrapper").css("display","none");
        };

		// Navigation services

		NavigationService.getalldatadesc('posts', 'datetime').then(getalldatasuccess, getalldataerror);


  }
]);

phonecatControllers.controller('createpostCtrl', ['$scope', 'TemplateService', 'NavigationService', '$routeParams', '$location', 'FileUploader', '$http',
  function ($scope, TemplateService, NavigationService, $routeParams, $location, FileUploader, $http) {
		$scope.template = TemplateService;
		TemplateService.content = "views/createpost.html";
		$scope.title = "createpost";

		// Initializations
		var editid = $routeParams.postid;
		console.log("Edit ID", editid);
		$scope.post = {};


		// Local functions


		uploadimagesuccess = function (response) {
			console.log('upload image success');
			$scope.post.image = response.data;
			if (editid > 0) {
				NavigationService.editdata('posts', $scope.post).then(addpostsuccess, addposterror);
			} else {
				NavigationService.storepostdata('posts',$scope.post).then(addpostsuccess, addposterror);
			};
		}
		uploadimageerror = function (response) {
			console.log('Internet Error');
		}
		getdatabyidsuccess = function (response) {
			console.log(response);
			$scope.post = response.data;
			$scope.post.datetime = new Date($scope.post.datetime);
		}
		getdatabyiderror = function (error) {
			console.log('Internet Error');
		}

		// Scope functions
		var addpostsuccess = function (response) {
			console.log(response.data);
			$location.path('posts');
		};
		var addposterror = function (response) {
			console.log(response.data);
		};
		$scope.addpost = function () {
		    $scope.post.datetime = $('#postdateinput').val();
			if (editid > 0) {
				if ($scope.post.image != '') {
					NavigationService.uploadimage({
						"path": "posts",
						"image": $scope.post.image
					}).then(uploadimagesuccess, uploadimageerror);
				} else {
					console.log($scope.post);
					NavigationService.editdata('posts', $scope.post).then(addpostsuccess, addposterror);
				};
			} else {
				if ($scope.post.image != '') {
					NavigationService.uploadimage({
						"path": "posts",
						"image": $scope.post.image
					}).then(uploadimagesuccess, uploadimageerror);
				} else {
					NavigationService.storepostdata('posts',$scope.post).then(addpostsuccess, addposterror);
				};
			};
		};

		if (editid > 0) {
			console.log("WE ARE IN EDIT PAGE");
			NavigationService.getdatabyid('posts', editid).then(getdatabyidsuccess, getdatabyiderror);
		};
		$scope.cancelaction = function () {
			$location.path('posts');
		};

		// Navigation services functions


  }
]);

phonecatControllers.controller('fixturesCtrl', ['$scope', 'TemplateService', 'NavigationService', '$location', '$http', '$rootScope', '$interval',
  function ($scope, TemplateService, NavigationService, $location, $http, $rootScope, $interval) {
		$scope.template = TemplateService;
		TemplateService.content = "views/fixtures.html";
		$scope.title = "fixtures";


		// Initializations
		$scope.fixtures = [];

		// callbacks
		getalldatasuccess = function (response) {
			$scope.fixtures = response.data;
		}
		getalldataerror = function (error) {
			console.log('Internet Error');
		}

		// SCOPE function
		$scope.deletefixture = function (fixture) {
			NavigationService.deletedatabyid('fixtures', fixture.id).then(function (response) {
				$scope.fixtures.splice($scope.fixtures.indexOf(fixture), 1);
			}, function (error) {
				console.log('Internet Error');
			});

		};


		$scope.gotocreatefixturepage = function (id) {
			$location.path('createfixtures/' + id);
		}

		// Navigation services
		// NavigationService.getalldata('fixtures').then(getalldatasuccess, getalldataerror);





  }
]);

phonecatControllers.controller('galleryCtrl', ['$scope', 'TemplateService', 'NavigationService', '$location', '$routeParams', 'textAngularManager', '$rootScope', '$interval', '$anchorScroll',
  function ($scope, TemplateService, NavigationService, $location, $routeParams, textAngularManager, $rootScope, $interval, $anchorScroll) {
		$scope.template = TemplateService;
		TemplateService.content = "views/gallery.html";
		$scope.title = "gallery";


		/*Initializations*/

		$scope.albums = [];

		$scope.photos = {};
		$scope.upload = {};
		$scope.photocollections = {};
		var initializeslider = function (albumid) {

			setTimeout(function () {
				console.log(albumid);
				$('#photoslider' + albumid).slick({
					slidesToShow: 4,
					slidesToScroll: 1,
					nextArrow: '<div class="photos-next-arrow photos-slider-arrow"> <svg enable-background="new 0 0 451.846 451.847" version="1.1" viewBox="0 0 451.85 451.85" xml:space="preserve" xmlns="http://www.w3.org/2000/svg"><path d="m345.44 248.29-194.29 194.28c-12.359 12.365-32.397 12.365-44.75 0-12.354-12.354-12.354-32.391 0-44.744l171.91-171.91-171.91-171.9c-12.354-12.359-12.354-32.394 0-44.748 12.354-12.359 32.391-12.359 44.75 0l194.29 194.28c6.177 6.18 9.262 14.271 9.262 22.366 0 8.099-3.091 16.196-9.267 22.373z"/></svg> </div>',
					prevArrow: '<div class="photos-prev-arrow photos-slider-arrow"> <svg enable-background="new 0 0 451.846 451.847" version="1.1" viewBox="0 0 451.85 451.85" xml:space="preserve" xmlns="http://www.w3.org/2000/svg"><path d="m345.44 248.29-194.29 194.28c-12.359 12.365-32.397 12.365-44.75 0-12.354-12.354-12.354-32.391 0-44.744l171.91-171.91-171.91-171.9c-12.354-12.359-12.354-32.394 0-44.748 12.354-12.359 32.391-12.359 44.75 0l194.29 194.28c6.177 6.18 9.262 14.271 9.262 22.366 0 8.099-3.091 16.196-9.267 22.373z"/></svg> </div>',
					infinite: false
				});
			}, 1000);
			/* ADD A PHOTO TO SLIDER */
			/* $('.photos-list').slick('slickAdd', '<li class="photo-item"><img src="https://www.thegamecrafter.com/overlays/largesquareboard.png" style="background-image:url()" /></li>');
			  REMOVE A PHOTO FROM SLIDER 
			 $('.photos-list').slick('slickRemove', '<li class="photo-item"><img src="https://www.thegamecrafter.com/overlays/largesquareboard.png" style="background-image:url(https://images.pexels.com/photos/46710/pexels-photo-46710.jpeg)" /></li>');*/

		}


		/*SCOPE FUNCTION*/
		$scope.createalbum = function () {
			$scope.albums.push({
				'name': ''
			})
		}


		/*Local Functions*/
		var getphotosbycategoryid = function (photo) {
			return photo.album_id == this;
		}
		getalbumssuccess = function (response) {
			$scope.albums = response.data;

			console.log('');
			NavigationService.getalldata('photos').then(function (response) {
				console.log(response);
				_.forEach($scope.albums, function (value, key) {

					$scope.photos[value.id] = response.data.filter(getphotosbycategoryid, value.id);
					$scope.photocollections[value.id] = angular.copy($scope.photos[value.id]);
					initializeslider(value.id);
				});
				console.log($scope.photos);
			}, function (error) {

			});
			console.log(response);
		}
		getalbumserror = function (error) {
			console.log(error)
		}
		$scope.storealbum = function (album) {

			storepostdatasuccess = function (response) {
				album.id = response.data;
				initializeslider(album.id);
				console.log(response);
			}
			storepostdataerror = function (error) {
				console.log(error)
			}


			NavigationService.storepostdata('albums', album).then(storepostdatasuccess, storepostdataerror)
		}


		$scope.openfilechooser = function (index, album) {

			document.getElementById('fileInput' + index).click();
		}


		deletephoto = function (photoid, photoname, albumid) {

			deletedatabyidsuccess = function (response) {
				NavigationService.deleteimage({
					path: 'gallery',
					filename: photoname
				}).then(function (response) {
					console.log(response);
				}, function (error) {

				});
				console.log($scope.photocollections[albumid], photoid);
				var index = $scope.photocollections[albumid].findIndex(photo => photo.id == photoid);
				$scope.photocollections[albumid].splice(index, 1);
				console.log(index);
				$('#photoslider' + albumid).slick('slickRemove', index + 1);
				console.log(response);
			}
			deletedatabyiderror = function (error) {
				console.log(error)
			}

			console.log($('#photoslider' + albumid));
			NavigationService.deletedatabyid('photos', photoid).then(deletedatabyidsuccess, deletedatabyiderror);
		}
		$scope.deletealbum = function (album) {

			deletedatabyidsuccess = function (response) {
				$scope.albums.splice($scope.albums.indexOf(album), 1);

				console.log(response);
			}
			deletedatabyiderror = function (error) {
				console.log(error)
			}


			NavigationService.deletedatabyid('albums', album.id).then(deletedatabyidsuccess, deletedatabyiderror)
		};



		$scope.uploadimage = function (album) {
			console.log(typeof $scope.photos[album.id]);
			if ((typeof $scope.photos[album.id]) == "undefined") {

				$scope.photos[album.id] = [];
			}
			uploadimagesuccess = function (response) {
				console.log(response);
				var photo = {
					name: response.data,
					album_id: album.id
				};
				NavigationService.storepostdata('photos', photo).then(function (response) {
					console.log(response);
					photo.id = response.data;



					if (typeof $scope.photocollections[album.id] == "undefined") {
						$scope.photocollections[album.id] = [];
					}

					$scope.photocollections[album.id].push(photo);



					$('#photoslider' + album.id).slick('slickAdd', `<li class="photo-item" >
                    <div class="photo-actions" onclick="deletephoto(` + photo.id + `,'` + photo.name + `',` + album.id + `)">
                        <svg enable-background="new 0 0 774.266 774.266" version="1.1" viewBox="0 0 774.27 774.27" xml:space="preserve" xmlns="http://www.w3.org/2000/svg">
                            <path d="m640.35 91.169h-103.38v-67.178c0-13.522-10.907-23.991-24.428-23.991-1.312 0-2.187 0.438-2.614 0.875-0.438-0.437-1.313-0.875-1.75-0.875h-246.46c-13.521 0-23.99 10.469-23.99 23.991v67.179h-103.82c-29.667 0-52.783 23.116-52.783 52.783v86.368h45.803v491.6c0 29.668 22.679 52.346 52.346 52.346h415.7c29.667 0 52.782-22.678 52.782-52.346v-491.6h45.366v-86.368c0-29.667-23.125-52.784-52.783-52.784zm-354.64-43.188h202.84v43.188h-202.84v-43.188zm313.64 673.94c0 3.061-1.312 4.363-4.364 4.363h-415.7c-3.052 0-4.364-1.303-4.364-4.363v-491.6h424.43v491.6zm45.366-539.58h-515.16v-38.387c0-3.053 1.312-4.802 4.364-4.802h506.44c3.053 0 4.365 1.749 4.365 4.802v38.387z" />
                            <rect x="475.03" y="286.59" width="48.418" height="396.94" />
                            <rect x="363.36" y="286.59" width="48.418" height="396.94" />
                            <rect x="251.69" y="286.59" width="48.418" height="396.94" />
                        </svg>
                        <p class="action-text">DELETE</p>
                    </div>
                    <img src="https://www.thegamecrafter.com/overlays/largesquareboard.png" style="background-image:url(` + imageurl + `gallery/` + photo.name + `)" />
                </li>`);


					console.log($scope.photocollections[album.id]);
				}, function (error) {

				});
				console.log(response);
			}
			uploadimageerror = function (error) {
				console.log(error)
			}


			NavigationService.uploadimage({
				path: "gallery",
				image: $scope.upload.name
			}).then(uploadimagesuccess, uploadimageerror)
		}






		NavigationService.getalldata('albums').then(getalbumssuccess, getalbumserror)
                    }

                    ]);

phonecatControllers.controller('groupsCtrl', ['$scope', 'TemplateService', 'NavigationService', '$location', '$routeParams', 'textAngularManager', 'FileUploader', '$filter', '$rootScope', '$window', '$interval', '$anchorScroll',
  function ($scope, TemplateService, NavigationService, $location, $routeParams, textAngularManager, FileUploader, $filter, $rootScope, $window, $interval, $anchorScroll) {
		$scope.template = TemplateService;
		TemplateService.content = "views/groups.html";
		$scope.title = "groups";

		//      Initialization

		var categoryid = $routeParams.categoryid;

		$scope.groups = [];
		var originaldata = {};

		//      Callbacks
		getmanybysuccess = function (response) {
			$scope.groups = _.map(response.data, function (element) {
				Object.defineProperty(element, 'name', {
					writable: false
				});
				return element;
			});
			console.log(response);
		}

		getmanybyerror = function (error) {
			console.log('Internet Error');
		}






		/*Scope functions*/
		$scope.deletegroup = function (group) {

			deletedatabyidsuccess = function (response) {
				console.log(response);
				$scope.groups.splice($scope.groups.indexOf(group), 1);
			}
			deletedatabyiderror = function (error) {
				console.log('Internet Error');
			}

			NavigationService.deletedatabyid('groups', group.id).then(deletedatabyidsuccess, deletedatabyiderror);
		}
		$scope.savegroup = function (group) {
			storepostdatasuccess = function (response) {
				console.log(response);
				if (!group.id) {
					group.id = response.data;

				}
				Object.defineProperty(group, 'name', {
					writable: false
				});
			}
			storepostdataerror = function (error) {
				console.log('Internet Error');
			}

			if (group.name) {
				if (group.id) {
					NavigationService.editdata('groups', {
						'id': group.id,
						'data': angular.toJson(group)
					}).then(storepostdatasuccess, storepostdataerror);
				} else {
					group.category_id = categoryid;
					NavigationService.storepostdata('groups', group).then(storepostdatasuccess, storepostdataerror);
				}
			}
		};

		$scope.canceladdition = function (group) {
			if (group.id) {
				group.name = originaldata[group.id].name;
				Object.defineProperty(group, 'name', {
					writable: false
				});

			} else {
				var index = $scope.groups.indexOf(group);
				$scope.groups.splice(index, 1);
			}


		};

		$scope.editgroup = function (group) {
			originaldata[group.id] = angular.copy(group);
			Object.defineProperty(group, 'name', {
				writable: true
			});

		}


		//        $scope.gototeampage = function (groupid) {
		//            $location.path('teams/' + groupid);
		//        };

		/*Navigation functions*/
		NavigationService.getmanyby('groups', 'category_id', categoryid).then(getmanybysuccess, getmanybyerror);



  }
]);

phonecatControllers.controller('photouploadCtrl', ['$scope', 'TemplateService', 'NavigationService', '$routeParams', '$location',
  function ($scope, TemplateService, NavigationService, $routeParams, $location) {
		$scope.template = TemplateService;
		TemplateService.content = "views/photoupload.html";
		$scope.title = "photoupload";


  }
]);



phonecatControllers.controller('teamsCtrl', ['$scope', 'TemplateService', 'NavigationService', '$location', '$routeParams',
  function ($scope, TemplateService, NavigationService, $location) {
		$scope.template = TemplateService;
		TemplateService.content = "views/teams.html";
		$scope.title = "teams";

		// Initializations

		$scope.teams = [];



		// callbacks
		getalldatasuccess = function (response) {
			$scope.teams = response.data;
		}

		getalldataerror = function (error) {
			console.log(error);
		};
		NavigationService.getalldata('teams').then(getalldatasuccess, getalldataerror);



		// SCOPE FUNCTIONS
		$scope.gotocreateteampage = function (id) {
			$location.path('createteam/' + id);

		}
		$scope.deleteteam = function (team) {
			NavigationService.deletedatabyid('teams', team.id).then(function (response) {
				$scope.teams.splice($scope.teams.indexOf(team), 1);
			}, function () {});
		}
}]);

phonecatControllers.controller('headerctrl', ['$scope', 'TemplateService', '$location', '$rootScope', 'NavigationService',
  function ($scope, TemplateService, $location, $rootScope, NavigationService) {

		$scope.template = TemplateService;
		$scope.logout = function () {
			$.jStorage.flush();
			$location.path('login');
		}


  }
]);

phonecatControllers.controller('notificationsCtrl', ['$scope', 'TemplateService', '$location', '$rootScope', 'NavigationService',
  function ($scope, TemplateService, $location, $rootScope, NavigationService) {


		$scope.template = TemplateService;
		TemplateService.content = "views/notifications.html";
		$scope.title = "Notifications";



  }
]);
