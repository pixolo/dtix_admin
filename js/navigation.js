var navigationservice = angular.module('navigationservice', [])

    .factory('NavigationService', function ($http) {

        //MACBOOK AND HOME LAPTOP
        // var adminurl = "http://localhost/rest/rest/index.php/";
        //PC
        //var adminurl = "http://localhost/inqrest/rest/index.php/";
        //SERVER
        var adminurl = "http://www.dtix.org/dtix_admin/rest/index.php/";

        var getformdata = function (objectdata) {
            var formdata = new FormData();
            for (var key in objectdata) {
                console.log(key);
                formdata.append(key, objectdata[key]);
            }
            return formdata;
        }

        return {
            getnav: function () {
                return navigation;
            },
            makeactive: function (menuname) {
                for (var i = 0; i < navigation.length; i++) {
                    if (navigation[i].name == menuname) {
                        navigation[i].classis = "active";
                    } else {
                        navigation[i].classis = "";
                    }
                }
                return menuname;
            },
            showerror: function (msg) {
                // SHOW ERROR FUNCTION TO COME HERE
            },

            login: function (adminpaneluser) {

                var formdata = getformdata(adminpaneluser);
                console.log(formdata);
                return $http({
                    url: adminurl + 'users/dologin',
                    method: 'POST',
                    headers: {
                        'Content-Type': undefined
                    },
                    data: formdata,
                    transformRequest: angular.identity

                });

            },
            isloggedin: function (email) {
                return $http.get(adminurl + 'users/isloggedin', {
                    params: {
                        email: email
                    }
                });
            },
            uploadimage: function (imagedata) {
                console.log(imagedata);
                var formdata = getformdata(imagedata);
                return $http({
                    url: adminurl + 'posts/uploadImage',
                    method: "POST",
                    headers: {
                        'Content-Type': undefined,
                    },
                    data: formdata,
                    transformRequest: angular.identity
                });
            },
            deleteimage: function (imagedata) {
                console.log(imagedata);
                var formdata = getformdata(imagedata);
                return $http({
                    url: adminurl + 'teams/deleteImage',
                    method: "POST",
                    headers: {
                        'Content-Type': undefined,
                    },
                    data: formdata,
                    transformRequest: angular.identity
                });
            },
            getalldata: function (controllername) {
                console.log('called');
                return $http({
                    url: adminurl + controllername + '/getall',
                    method: "GET",
                })
            },
            getalldatadesc: function (controllername, columnname) {
                console.log('called');
                return $http({
                    url: adminurl + controllername + '/getalldesc',
                    method: "GET",
                    params: {
                        "column": columnname
                    }
                })
            },
            editdata: function (controllername, datatobeedited) {
                console.log(datatobeedited);
                //var formdata = getformdata(datatobeedited);
                //console.log(formdata.get('id'))
                return $http({
                    url: adminurl + controllername + '/updatebyid',
                    method: 'GET',
                    params: {id:datatobeedited.id,data:JSON.stringify(datatobeedited)}
                })

            },
            storepostdata: function (controllername, data) {
                var formdata = getformdata({
                    "data": angular.toJson(data)
                });
                return $http({
                    url: adminurl + controllername + '/postinsert',
                    method: 'POST',
                    headers: {
                        'Content-Type': undefined
                    },
                    data: formdata,
                    transformRequest: angular.identity

                })
            },
            deletedatabyid: function (controllername, id) {
                return $http.get(adminurl + controllername + '/deletebyid', {
                    params: {
                        id: id
                    }
                });

            },

            getdatabyid: function (controllername, id) {
                return $http.get(adminurl + controllername + '/getbyid', {
                    params: {
                        id: id
                    }
                });

            },
            getdataforfixtures: function () {

                return $http.get('http://pixoloproductions.com/fcb/fcbescolarest/index.php/tournaments/gettournamentinfodata', {
                    params: {
                        tournament_id: 24
                    }
                });
            },
            getfixturesbytournamentid: function (tournamentid) {
                return $http.get(adminurl + 'fixtures/getfixturesbytournamentid', {
                    params: {
                        tournament_id: tournamentid
                    }
                });
            },
            submitresult: function (result) {
                return $http.get(adminurl + 'results/updateresult', {
                    params: result
                });
            },
            getmanyby: function (controllername, field, value) {
                return $http.get(adminurl + controllername + '/getmanyby', {
                    params: {
                        field: field,
                        value: value
                    }
                });

            },
            getteams: function () {
                //

                return $http.get(adminurl + 'teams/getteamsforgroup', {
                    params: {
                        //                        tournament_id:$.jStorage.get("tournament");
                        tournament_id: 24
                    }
                });

            },
            getgroupteam: function (groupid) {
                return $http.get(adminurl + 'groups/getGroupTeams', {
                    params: {
                        group_id: groupid
                    }
                });
            },

            addpost: function(post){
                console.log(post);
                return $http.get(adminurl + 'posts/insert', {
                    params: {
                        data: JSON.stringify(post).replace(/\/\//g,"\\/\\/")
                    }
                });
            },


            // FIXTURE CREATION API's 
            deletefixturebyid: function (fixtureid) {
                return $http.get(adminurl + 'fixtures/deletebyid', {
                    params: {
                        id: fixtureid
                    }
                });
            },
            createfixture: function (fixtureobj) {
                var formdata = getformdata({
                    "data": angular.toJson(fixtureobj)
                });
                return $http({
                    url: adminurl + controllername + '/postinsert',
                    method: 'POST',
                    headers: {
                        'Content-Type': undefined
                    },
                    data: formdata,
                    transformRequest: angular.identity

                })
            },


        }
    });
